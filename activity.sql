SHOW DATABASES;

-- Create the blog_db database
CREATE DATABASE blog_db;

-- Switch to the blog_db database
USE blog_db;

-- Create the users table
CREATE TABLE users (
  id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(100) NOT NULL,
  password VARCHAR(300) NOT NULL,
  datetime_created DATETIME NOT NULL,
  PRIMARY KEY(id)
);

-- Create the posts table
CREATE TABLE posts (
  id INT NOT NULL AUTO_INCREMENT,
  author_id INT NOT NULL,
  title VARCHAR(500) NOT NULL,
  content VARCHAR(500) NOT NULL,
  datetime_posted DATETIME NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (author_id) REFERENCES users(id)
);

-- Create the post_comments table
CREATE TABLE post_comments (
  id INT NOT NULL AUTO_INCREMENT,
  post_id INT NOT NULL,
  user_id INT NOT NULL,
  content VARCHAR(500) NOT NULL,
  datetime_commented DATETIME NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (post_id) REFERENCES posts(id),
  FOREIGN KEY (user_id) REFERENCES users(id)
);

-- Create the post_likes table
CREATE TABLE post_likes (
  id INT NOT NULL AUTO_INCREMENT,
  post_id INT NOT NULL,
  user_id INT NOT NULL,
  datetime_liked DATETIME NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (post_id) REFERENCES posts(id),
  FOREIGN KEY (user_id) REFERENCES users(id)
);

